#
# Copyright (C) 2023 BlissLabs
#
# Licensed under the GNU General Public License Version 2 or later.
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.gnu.org/licenses/gpl.html
#
LOCAL_PATH := $(call my-dir)

#build tree
include $(CLEAR_VARS)
LOCAL_CFLAGS := -std=c11 -pedantic -Wall -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -Wno-unused-parameter
LOCAL_SRC_FILES := $(call all-c-files-under,.)

LOCAL_MODULE := tree
LOCAL_MODULE_TAGS := optional
LOCAL_C_INCLUDES:= $(LOCAL_PATH)
include $(BUILD_EXECUTABLE)
